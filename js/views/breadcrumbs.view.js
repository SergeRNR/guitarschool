define([
    'jquery',
    'underscore',
    'backbone',
    'helper'
], ($, _, Backbone, helper) => {
    let BreadcrumbsView = Backbone.View.extend({
        el: $('.breadcrumb'),
        render() {
            let section = helper.getSectionLink(this.attributes.section);
            let article = helper.getArticleLink(this.attributes.section, this.attributes.article);

            helper.getTemplate('breadcrumbs.html').then(template => {
                template = _.template(template);
                let content = template({
                    link: section.link,
                    section: (this.attributes.section == 'intro' ? null : section.tr),
                    article: article.tr
                });

                this.$el.html(content);
            });
        }
    });

    return BreadcrumbsView;
});
