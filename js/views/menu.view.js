define([
    'jquery',
    'underscore',
    'backbone',
    'helper'
], ($, _, Backbone, helper) => {
    let MenuView = Backbone.View.extend({
        el: $('.menu'),
        render() {
            helper.getTemplate('menu.html').then(template => {
                template = _.template(template);
                let content = template({ menu: helper.getContentStructure() });
                this.$el.html(content);
            });
        }
    });

    return MenuView;
});
