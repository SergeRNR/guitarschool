define([
    'jquery',
    'underscore',
    'backbone',
    'helper'
], ($, _, Backbone, helper) => {
    let ArticleView = Backbone.View.extend({
        el: $('.article'),
        render() {
            let htmlExt = '.htm';
            let templatePath = [
                this.attributes.section,
                this.attributes.article
            ].join('/') + htmlExt;

            helper.getTemplate(templatePath).then(template => {
                this.$el.html(template);
                $('body').animate({ scrollTop: 0 }, 500, 'swing');
            });
        }
    });

    return ArticleView;
});
