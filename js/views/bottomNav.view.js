define([
    'jquery',
    'underscore',
    'backbone',
    'helper'
], ($, _, Backbone, helper) => {
    let BottomNavView = Backbone.View.extend({
        el: $('.bottom-nav'),
        render() {
            let prev = helper.getPrevArticleLink(this.attributes.section, this.attributes.article);
            let next = helper.getNextArticleLink(this.attributes.section, this.attributes.article);

            helper.getTemplate('bottomNav.html').then(template => {
                template = _.template(template);
                let content = template({
                    prev,
                    next
                });
                this.$el.html(content);
            });
        }
    });

    return BottomNavView;
});
