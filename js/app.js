define([
    'router',
    'views/menu.view'
], (Router, MenuView) => {
    let initialize = () => {
        let menuView = new MenuView();
        menuView.render();
        Router.initialize();
    };

    return {
        initialize
    };
});
