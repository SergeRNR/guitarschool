require.config({
    paths: {
        jquery: 'libs/jquery',
        jqueryui: 'libs/jquery-ui',
        underscore: 'libs/underscore',
        backbone: 'libs/backbone'
    }
});

require(['app'], App => {
    App.initialize();
});
