define([
    'jquery',
    'underscore'
], function ($, _) {
    let emptyBreadcrumbs = { link: '', tr: '' };

    let helper = {

        getTemplate(templatePath) {
            return $.ajax(`js/templates/${templatePath}`);
        },

        getSectionOrder() {
            return [
                'intro',
                'elguitar',
                'theory',
                'fsteps',
                'picking',
                'rhythm',
                'solo',
                'rifflib',
                'sololib'
            ];
        },

        getContentStructure() {
            return {
                intro: {
                    link: 'intro',
                    tr: 'Вступление',
                    index: 'intro',
                    items: [
                        {link:'intro', tr:'Вступление'}
                    ]
                },
                elguitar: {
                    link: 'elguitar',
                    tr: 'Электрогитара',
                    index: 'guitar',
                    items: [
                        {link:'guitar', tr:'Устройство гитары'},
                        {link:'types', tr:'Виды гитар'},
                        {link:'choice', tr:'Выбор инструмента'},
                        {link:'amps', tr:'Усилители'},
                        {link:'pedals', tr:'Эффекты'},
                        {link:'picks', tr:'Медиаторы'},
                        {link:'cables', tr:'Шнуры'}
                    ]
                },
                theory: {
                    link: 'theory',
                    tr: 'Теория',
                    index: 'forwhat',
                    items: [
                        {link:'forwhat', tr:'Для чего нужно изучать теорию'},
                        {link:'scale', tr:'Звукоряд'},
                        {link:'notes', tr:'Нотное письмо'},
                        {link:'rhythm', tr:'Метр и ритм'},
                        {link:'tempo', tr:'Темп'},
                        {link:'interval', tr:'Интервалы'},
                        {link:'chords', tr:'Построение аккордов'},
                        {link:'harmony', tr:'Лады'},
                        {link:'swing', tr:'Свинг'},
                        {link:'guitarnotes', tr:'Расположение нот на грифе'},
                        {link:'tabs', tr:'Табулатура'},
                        {link:'chordsgrid', tr:'Аккордовые сетки'},
                        {link:'special', tr:'Специальные обозначения'}
                    ]
                },
                fsteps: {
                    link: 'fsteps',
                    tr: 'Первые шаги',
                    index: 'setup',
                    items: [
                        {link:'setup', tr:'Настройка гитары'},
                        {link:'sitting', tr:'Посадка за гитарой'},
                        {link:'rhand', tr:'Постановка правой руки'},
                        {link:'lhand', tr:'Постановка левой руки'},
                        {link:'howtowork', tr:'Как работать над упражнениями'},
                        {link:'pickdown', tr:'Удар медиатором вниз'},
                        {link:'powerchords', tr:'Power-аккорды и Palm muting'}
                    ]
                },
                picking: {
                    link: 'picking',
                    tr: 'Переменное звукоизвлечение',
                    index: 'picking',
                    items: [
                        {link:'picking', tr:'Переменное звукоизвлечение'},
                        {link:'c2', tr:'2-х октавная гамма До мажор'},
                        {link:'cfull', tr:'Гамма До мажор по всему грифу'},
                        {link:'chromatism', tr:'Хроматизмы'}
                    ]
                },
                rhythm: {
                    link: 'rhythm',
                    tr: 'Ритм-гитара',
                    index: 'rguitar',
                    items: [
                        {link:'rguitar', tr:'Ритм-гитара'},
                        {link:'chordforms', tr:'Аккордовые формы'},
                        {link:'riff', tr:'Что такое рифф'},
                        {link:'ariff', tr:'Риффы в тональности А'},
                        {link:'riffrepeat', tr:'Риффы с педальным тоном'},
                        {link:'riffblues', tr:'Блюзовые риффы в Е мажоре'},
                        {link:'riffsingle', tr:'Одноголосые риффы'},
                        {link:'riffchords', tr:'Аккордовые риффы'},
                        {link:'rhythmline', tr:'Построение ритм-партии'}
                    ]
                },
                solo: {
                    link: 'solo',
                    tr: 'Соло-гитара',
                    index: 'methods',
                    items: [
                        {link:'methods', tr:'Приемы игры'},
                        {link:'pentatonic1', tr:'Минорная пентатоника в 1 боксе'},
                        {link:'cliche', tr:'Рок-клише'},
                        {link:'pentatonic5', tr:'Минорная пентатоника в 5 боксах'},
                        {link:'pentatonicmajor', tr:'Мажорная пентатоника'},
                        {link:'hammerpull', tr:'Развитие техники hаmmer on - pull off'},
                        {link:'speed', tr:'Скоростные пассажи'},
                        {link:'tapping', tr:'Тэппинг'},
                        {link:'phrases', tr:'Фразировка'},
                        {link:'sololine', tr:'Построение рок-соло'}
                    ]
                },
                rifflib: {
                    link: 'rifflib',
                    tr: 'Библиотека риффов',
                    index: 'rifflib',
                    items: []
                },
                sololib: {
                    link: 'sololib',
                    tr: 'Библиотека соло',
                    index: 'sololib',
                    items: []
                }
            };
        },

        getSectionLink(section) {
            let content = this.getContentStructure()[section];

            if (content) {
                return { link: [content.link, content.index].join('/'), tr: content.tr };
            } else {
                return emptyBreadcrumbs;
            }
        },

        getArticleLink(section, article) {
            let content = this.getContentStructure()[section];

            if (content) {
                let search = _.where(content.items, { link: article });
                if (search.length) {
                    return { link: search[0].link, tr: search[0].tr };
                } else {
                    return emptyBreadcrumbs;
                }
            } else {
                return emptyBreadcrumbs;
            }
        },

        getPrevArticleLink(section, article) {
            return null;
        },

        getNextArticleLink(section, article) {
            let content = this.getContentStructure()[section];
            let articleIndex = content.items.findIndex(item => item.link === article);

            if (articleIndex === -1) {
                return null;
            } else if (articleIndex === content.items.length -1) {
                // find next section
                let sectionOrder = this.getSectionOrder();
                let sectionIndex = sectionOrder.indexOf(section);

                if (sectionIndex === sectionOrder.length - 1) {
                    return null;
                } else {
                    let nextSectionName = sectionOrder[sectionIndex + 1];
                    let nextSection = this.getContentStructure()[nextSectionName];
                    return `${nextSection.link}/${nextSection.index}`;
                }
                return null;
            } else {
                let nextArticle = content.items[articleIndex + 1];
                return `${section}/${nextArticle.link}`;
            }
        }
    };

    return helper;
});
