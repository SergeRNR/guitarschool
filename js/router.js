define([
    'jquery',
    'underscore',
    'backbone',
    'views/breadcrumbs.view',
    'views/article.view',
    'views/bottomNav.view'
], ($, _, Backbone, BreadcrumbsView, ArticleView, BottomNavView) => {
    let AppRouter = Backbone.Router.extend({
        routes: {
            '(/)': 'showArticle',
            ':section(/)': 'showArticle',
            ':section/:article(/)': 'showArticle'
        }
    });

    let initialize = () => {
        let router = new AppRouter;

        router.on('route:showArticle', (section, article) => {
            if (!section) {
                router.navigate('intro/intro', { trigger: true });
                return;
            }

            let breadcrumbsView = new BreadcrumbsView({
                attributes: { section, article }
            });
            breadcrumbsView.render();

            let articleView = new ArticleView({
                attributes: { section, article }
            });
            articleView.render();

            let bottomNavView = new BottomNavView({
                attributes: { section, article }
            });
            bottomNavView.render();
        });

        Backbone.history.start();
    };

    return {
        initialize
    };
});
