let app = require('./app');

let ipaddress = process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
let port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.listen(port, ipaddress, () => {
	console.log(`Server started on ${ipaddress}:${port}`);
});
